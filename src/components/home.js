import React from 'react';
import { PayPalScriptProvider, PayPalButtons } from '@paypal/react-paypal-js';

const Home = () => {
  const createOrder = (data, actions) => actions.order.create({
    purchase_units: [{
      amount: {
        currency_code: 'USD',
        value: '100',
        breakdown: {
          item_total: {
            currency_code: 'USD',
            value: '100'
          }
        }
      },
      items: [{
        name: 'First Product Name',
        description: 'Optional descriptive text..',
        unit_amount: {
          currency_code: 'USD',
          value: '50'
        },
        quantity: '2'
      }]
    }]
  });

  const onApprove = (data, actions) => actions.order.capture();

  return (
    <>
      <PayPalScriptProvider options={{ 'client-id': 'Ac4a9x3oO1zsewkKVzYe9C_u5xnb1AK-ftFs9LgHYEDWiajMEn4IR9Vc_EDoJMHjHwWr7IVRgZUFqX0J' }}>
        <PayPalButtons
          style={{ layout: 'horizontal' }}
          createOrder={(data, actions) => createOrder(data, actions)}
          onApprove={(data, actions) => onApprove(data, actions)}
        />
      </PayPalScriptProvider>
    </>
  );
};

export default Home;
